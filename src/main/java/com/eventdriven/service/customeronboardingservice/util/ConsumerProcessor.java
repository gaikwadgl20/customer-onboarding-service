package com.eventdriven.service.customeronboardingservice.util;

import com.eventdriven.service.customeronboardingservice.gateway.ConsumeMessage;
import com.eventdriven.service.customeronboardingservice.gateway.ProduceMessage;
import com.eventdriven.service.customeronboardingservice.model.CustomerDetails;
import com.eventdriven.service.customeronboardingservice.model.CustomerDetailsRoot;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConsumerProcessor {

    private static final Logger logger = LoggerFactory.getLogger(ConsumerProcessor.class);

    @Autowired
    CustomerDetailsBuilder customerDetailsBuilder;

    @Autowired
    ProduceMessage produceMessage ;


    @Value("${kafka.topic.customer-res-topic}")
    private String customerOnboardingResponseTopic;

    @Value("${kafka.topic.customer-err-topic}")
    private String customerOnboardingFailureTopic;

    public void processConsumedRecords(ConsumerRecord<String, Object> consumerRecord) {
        CustomerDetailsRoot customerDetailsRoot = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);



        try {
            String value = consumerRecord.value().toString();
            logger.info("Record received: {}", value);


            logger.info("Record key from consumed record: {}", consumerRecord.key());
            logger.info("Record value from consumed record: {}", consumerRecord.value());
            customerDetailsRoot = mapper.readValue(value, CustomerDetailsRoot.class);
            logger.info("Received CustomerDetailsObject: {}", customerDetailsRoot);
            processCustomerDetailsObject(customerDetailsRoot, consumerRecord.key());


        } catch (Exception jsonMappingException) {
            jsonMappingException.printStackTrace();
        }
    }

    private void processCustomerDetailsObject(CustomerDetailsRoot customerDetailsRoot, String key) {
        CustomerDetails customerDetails = null;
        customerDetails = customerDetailsRoot.getCustomerDetails();

        if (customerDetails != null) {
            if (customerDetails.getFirstName()==null || customerDetails.getLastName()==null ||
                    customerDetails.getAddress()==null || customerDetails.getContactNumber()==null ){
                logger.info("Customer details provided are not proper. Customer Details: {}", customerDetails );
               CustomerDetailsRoot customerDetailsErrorResponse= customerDetailsBuilder.customerDetailsErrorResponseBuilder(customerDetails);
                produceMessage.produceMessage(customerOnboardingFailureTopic,key,customerDetailsErrorResponse );
            }else {
                CustomerDetailsRoot customerDetailsResponse= customerDetailsBuilder.customerDetailsResponseBuilder(customerDetails);
                logger.info("Customer details provided are proper proceeding with customer registration.");
                produceMessage.produceMessage(customerOnboardingResponseTopic,key,customerDetailsResponse );
            }

        } else {
            logger.error("Customer Details object is not generated properly from consumed record.");
        }

    }
}
