package com.eventdriven.service.customeronboardingservice.util;


import com.eventdriven.service.customeronboardingservice.model.CustomerDetails;
import com.eventdriven.service.customeronboardingservice.model.CustomerDetailsRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CustomerDetailsBuilder {
    private static final Logger logger = LoggerFactory.getLogger(CustomerDetailsBuilder.class);



    @Value("${kafka.topic.customer-req-topic}")
    private String customerOnboardingRequestTopic;

    @Value("${customer.onboarding.message}")
    private String customerOnboardingSuccessMessage;

    @Value("${customer.onboarding.failureMessage}")
    private String customerOnboardingErrorMessage;


    public CustomerDetailsRoot customerDetailsBuilder(String fName, String lName, String address, String contactNo){
        CustomerDetailsRoot customerDetailsRoot = new CustomerDetailsRoot();

        CustomerDetails customerDetails= new CustomerDetails();
        customerDetails.setFirstName(fName);
        customerDetails.setLastName(lName);
        customerDetails.setAddress(address);
        customerDetails.setContactNumber(contactNo);

        customerDetailsRoot.setCustomerDetails(customerDetails);
        return customerDetailsRoot;
    }

    public CustomerDetailsRoot customerDetailsResponseBuilder(CustomerDetails customerDetailsReceived){
        CustomerDetailsRoot customerDetailsRoot = new CustomerDetailsRoot();

        CustomerDetails customerDetails= new CustomerDetails();
        String uniqueID= UUID.randomUUID().toString();
        customerDetails.setCustomerId(uniqueID);
        customerDetails.setFirstName(customerDetailsReceived.getFirstName());
        customerDetails.setLastName(customerDetailsReceived.getLastName());
        customerDetails.setAddress(customerDetailsReceived.getAddress());
        customerDetails.setContactNumber(customerDetailsReceived.getContactNumber());
        customerDetails.setMessage(customerOnboardingSuccessMessage);

        logger.info("CustomerDetails Response Object: {}",customerDetails);
        customerDetailsRoot.setCustomerDetails(customerDetails);
        return customerDetailsRoot;
    }

    public CustomerDetailsRoot customerDetailsErrorResponseBuilder(CustomerDetails customerDetailsReceived){
        CustomerDetailsRoot customerDetailsRoot = new CustomerDetailsRoot();

        CustomerDetails customerDetails= new CustomerDetails();
        customerDetails.setFirstName(customerDetailsReceived.getFirstName());
        customerDetails.setLastName(customerDetailsReceived.getLastName());
        customerDetails.setAddress(customerDetailsReceived.getAddress());
        customerDetails.setContactNumber(customerDetailsReceived.getContactNumber());
        customerDetails.setMessage(customerOnboardingErrorMessage);

        logger.info("CustomerDetails Response Object: {}",customerDetails);
        customerDetailsRoot.setCustomerDetails(customerDetails);
        return customerDetailsRoot;
    }
}
