package com.eventdriven.service.customeronboardingservice.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class FormatObject {
    private static final Logger logger = LoggerFactory.getLogger(FormatObject.class);

    ObjectMapper objectMapper = new ObjectMapper();

    public String buildJsonMessage(Object obj) {
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        String msg = null;
        try {
            msg = objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException ex) {
            logger.error("Error: " + ex);
        }
        logger.info("Message: " + msg);
        return msg;
    }

    public String buildJsonMessageWithNull(Object obj) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        String msg = null;
        try {
            msg = mapper.writeValueAsString(obj);
        } catch (JsonProcessingException ex) {
            logger.error("Error: " + ex);
        }
        logger.info("Message: " + msg);
        return msg;
    }
}
