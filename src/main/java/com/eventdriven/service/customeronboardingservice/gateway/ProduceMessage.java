package com.eventdriven.service.customeronboardingservice.gateway;


import com.eventdriven.service.customeronboardingservice.model.CustomerDetailsRoot;
import com.eventdriven.service.customeronboardingservice.util.FormatObject;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

@Component
public class ProduceMessage {

    @Autowired
    FormatObject formatObject;


    private static final Logger logger = LoggerFactory.getLogger(ProduceMessage.class);


    public String produceMessage(String topic, String key, CustomerDetailsRoot customerDetailsRoot) {
        Properties producerProperties = new Properties();
        producerProperties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        producerProperties.setProperty(ProducerConfig.ACKS_CONFIG, "all");
        producerProperties.setProperty(ProducerConfig.RETRIES_CONFIG, "10");
        producerProperties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        producerProperties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        Producer<String, Object> producer = new KafkaProducer<>(producerProperties);

        ProducerRecord<String, Object> producerRecord =
                new ProducerRecord<String, Object>(topic, key, formatObject.buildJsonMessage(customerDetailsRoot));
        RecordMetadata recordMetadata = null;
        try {
            recordMetadata = producer.send(producerRecord).get();
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Error occurred while producing event: " + e.getLocalizedMessage());
            e.printStackTrace();
        }

        logger.info("Produced Record Details: {}", producerRecord);
        producer.flush();
        producer.close();
        logger.info("Produced Record Metadata: {}", recordMetadata);
        if (recordMetadata != null)
            return recordMetadata.toString();
        else
            return "Failed to produce event.";

    }


}
