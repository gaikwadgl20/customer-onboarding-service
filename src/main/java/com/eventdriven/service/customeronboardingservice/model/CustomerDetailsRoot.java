package com.eventdriven.service.customeronboardingservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CustomerDetailsRoot {
    @JsonProperty("customerDetails")
    public CustomerDetails getCustomerDetails() {
        return this.customerDetails; }
    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails; }
    CustomerDetails customerDetails;
}
