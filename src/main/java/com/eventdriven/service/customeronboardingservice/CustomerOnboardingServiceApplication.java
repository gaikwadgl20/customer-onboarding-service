package com.eventdriven.service.customeronboardingservice;

import com.eventdriven.service.customeronboardingservice.gateway.ConsumeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class CustomerOnboardingServiceApplication implements CommandLineRunner {

	@Autowired
	ConsumeMessage consumeMessage;

    public static void main(String[] args) {
        new SpringApplicationBuilder(CustomerOnboardingServiceApplication.class).run(args);
    }

    public void  run (String... args) throws Exception{
    	consumeMessage.startConsumer();
	}

}
